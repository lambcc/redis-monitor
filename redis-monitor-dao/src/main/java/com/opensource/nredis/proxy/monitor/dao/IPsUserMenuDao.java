package com.opensource.nredis.proxy.monitor.dao;

import com.opensource.nredis.proxy.monitor.model.PsUserMenu;


/**
* dao
*
* @author liubing
* @date 2016/12/20 18:45
* @version v1.0.0
*/
public interface IPsUserMenuDao extends IMyBatisRepository<PsUserMenu>,IPaginationDao<PsUserMenu> {
}
